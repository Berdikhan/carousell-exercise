# Carousell Exercise

## About
Digg and Reddit are sites powered by their users’ upvotes and downvotes. When a user makes a contribution to their website, other users may upvote or downvote a particular topic, giving rise to a set of topics that are popular and unpopular.

## Design Pattern
Design pattern used in the project - MVVM (Model-View-ViewModel)

## Code
I have created following MVVM objects: 
1. Topic (Model)
2. TopicViewModel (ViewModel) - all bindings are done using this class. Topic model is not accessible from Controller. ViewModel populates the actual View, which is our TableViewCell. 
Advantages of using MVVM : 
a. The translation code lives seperated of the ViewController
b. View Controllers more easily maintainable
c. Generates more testable code
d. Make chanes in the UI and Model easier
Disadvantages:
a. Could create memory issues

## Versioning
v.1.0

## Authors
Berdikhan Satenov
