//
//  AddTopicViewController.swift
//  Carousell Exercise
//
//  Created by Berdikhan on 11/04/2017.
//  Copyright © 2017 Berdikhan. All rights reserved.
//

import UIKit
import KMPlaceholderTextView

protocol AddTopicViewControllerDelegate {
    func didCreateTopic(topic : TopicViewModel)
}

class AddTopicViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate {

    var delegate : AddTopicViewControllerDelegate?
    
    @IBOutlet weak var txtTopicTitle: UITextField!
    @IBOutlet weak var txtTopicDescription: KMPlaceholderTextView!
    @IBOutlet weak var btnCreate: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.btnCreate.backgroundColor = UIColor.gray
        self.btnCreate.isEnabled = false
        self.btnCreate.addTarget(self, action: #selector(createTopic), for: UIControlEvents.touchUpInside)
        self.txtTopicDescription.delegate = self
        self.txtTopicTitle.delegate = self
        self.txtTopicTitle.addTarget(self, action: #selector(textfieldValueChanged(textfield:)), for: UIControlEvents.touchUpInside)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        // Do any additional setup after loading the view.
    }

    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textViewDidChange(_ textView: UITextView) {
        validate()
    }
    
    func textfieldValueChanged(textfield : UITextField){
        validate()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.txtTopicDescription.becomeFirstResponder()
        return false
    }
    
    func validate(){
        if (0 < (self.txtTopicTitle.text?.characters.count)! && (self.txtTopicTitle.text?.characters.count)! < 101
            && 0 < self.txtTopicDescription.text.characters.count && self.txtTopicDescription.text.characters.count < 256)
        {
            self.btnCreate.backgroundColor = UIColor.blue
            self.btnCreate.isEnabled = true
        }
        else
        {
            self.btnCreate.backgroundColor = UIColor.gray
            self.btnCreate.isEnabled = false
        }
    }

    func keyboardWillShow(notification : NSNotification)
    {
        let infos = notification.userInfo
        let value = infos?[UIKeyboardFrameEndUserInfoKey] as! NSValue
        let rawFrame = value.cgRectValue
        let keyboardFrame = self.view.convert(rawFrame, from: nil)
        var frame = self.view.frame
        frame.size.height = UIScreen.main.bounds.size.height - keyboardFrame.size.height
        self.view.frame = frame
    }

    func createTopic(){
        let topic = TopicViewModel(topic: Topic(title: self.txtTopicTitle.text!, description: self.txtTopicDescription.text, vote: 0))
        delegate?.didCreateTopic(topic: topic)
        self.navigationController?.popViewController(animated: true)
    }
    
}
