//
//  TopicTableViewCell.swift
//  Carousell Exercise
//
//  Created by Berdikhan on 10/04/2017.
//  Copyright © 2017 Berdikhan. All rights reserved.
//

import UIKit

class TopicTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblVote: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnUpVote: UIButton!
    @IBOutlet weak var btnDownVote: UIButton!
    
    // ViewModel 
    var topicViewModel : TopicViewModel? {
        didSet {
            self.lblVote?.text = self.topicViewModel?.voteText
            self.lblTitle?.text = self.topicViewModel?.title
            self.lblDescription?.text = self.topicViewModel?.description
            self.lblVote?.textColor = self.topicViewModel?.voteColor
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
