//
//  ViewController.swift
//  Carousell Exercise
//
//  Created by Berdikhan on 10/04/2017.
//  Copyright © 2017 Berdikhan. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, AddTopicViewControllerDelegate {

    @IBOutlet weak var tblTopics: UITableView!
    let identifier = "topicCellIdentifier"
    var topics : [TopicViewModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Auto height
        self.tblTopics.estimatedRowHeight = 100
        self.tblTopics.rowHeight = UITableViewAutomaticDimension
        
        // Instantiate Table view cell
        let nib = UINib(nibName: "TopicTableViewCell", bundle: nil)
        self.tblTopics.register(nib, forCellReuseIdentifier: identifier)
        
        // Set Delegate and DataSource
        self.tblTopics.delegate = self
        self.tblTopics.dataSource = self
        self.tblTopics.reloadData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblTopics.dequeueReusableCell(withIdentifier: self.identifier, for: indexPath) as! TopicTableViewCell
        let topic = topics[indexPath.row] as TopicViewModel
        
        // Set ViewModel of cell
        cell.topicViewModel = topic
        
        cell.btnUpVote.tag = indexPath.row
        cell.btnDownVote.tag = indexPath.row
        cell.btnUpVote.addTarget(self, action: #selector(upVote(sender:)), for: UIControlEvents.touchUpInside)
        cell.btnDownVote.addTarget(self, action: #selector(downVote(sender:)), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return topics.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func didCreateTopic(topic: TopicViewModel) {
        topics.append(topic)
        reloadTable()
    }
    
    func sortTopics(){
        self.topics.sort { (a, b) -> Bool in
            a.vote > b.vote
        }
    }
    
    func upVote(sender : UIButton){
        let topic = topics[sender.tag]
        topic.vote(increase: true)
        reloadTable()
    }
    
    func downVote(sender : UIButton){
        let topic = topics[sender.tag]
        topic.vote(increase: false)
        reloadTable()
    }
    
    @IBAction func createTopic(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil) as UIStoryboard
        let controller = storyboard.instantiateViewController(withIdentifier: "AddTopicViewController") as! AddTopicViewController
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func reloadTable(){
        sortTopics()
        self.tblTopics.reloadData()
    }
    
}

