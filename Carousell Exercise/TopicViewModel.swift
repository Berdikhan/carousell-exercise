//
//  TopicViewModel.swift
//  Carousell Exercise
//
//  Created by Berdikhan on 10/04/2017.
//  Copyright © 2017 Berdikhan. All rights reserved.
//

import UIKit

class TopicViewModel {
    
    private let topic : Topic
    
    var title : String {
        return self.topic.title!
    }
    
    var description : String {
        return self.topic.description!
    }
    // text color is red if negative, positive - green, darkGray - 0
    var voteColor : UIColor {
        return self.topic.vote > 0 ? UIColor.green : (self.topic.vote < 0 ? UIColor.red : UIColor.darkGray)
    }
    
    var voteText : String {
        return String(describing: self.topic.vote)
    }
    
    var vote : Int {
        return self.topic.vote
    }
    
    func vote(increase : Bool) {
        self.topic.vote += increase ? 1 : -1
    }
    
    init(topic : Topic) {
        self.topic = topic
    }
    
}
