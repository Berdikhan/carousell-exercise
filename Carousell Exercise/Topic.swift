//
//  Topic.swift
//  Carousell Exercise
//
//  Created by Berdikhan on 10/04/2017.
//  Copyright © 2017 Berdikhan. All rights reserved.
//

import Foundation

class Topic {
    var title : String?
    var description : String?
    var vote : Int
    
    required init(title : String, description : String, vote : Int) {
        self.title = title
        self.description = description
        self.vote = vote
    }
    
}
